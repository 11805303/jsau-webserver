'use strict'
const express = require('express')
const morgan = require('morgan')
//const file = require('/file.json')
const app = express()
app.use(morgan('dev'))

const port = 3000

const bunyan = require('bunyan')
const log = bunyan.createLogger({name: 'myapp'})

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.put('/', (req, res) => {
    res.send('bonjour World!')
})

app.listen(port, () => {
    log.info(`Example app listening at http://localhost:${port}`)
})
