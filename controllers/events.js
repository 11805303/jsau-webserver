'use strict'
const events = require('../ressource/events.json')
//const morgan = require('morgan')
const bunyan = require('bunyan')
//const fs = require('fs')
const fsp = require('fs').promises
const log = bunyan.createLogger({name: 'Agenda'})
const filePath = './ressource/events.json'

const getEvents = (req, res) => {
    fsp.readFile(filePath, 'utf8')
        .then(JSON.parse)
        .then((obj)=>{
            //res.send(obj)
            res.render('interfaces.html', {events:obj})

            res.status(200)
        })
        .catch((err)=>{
            log.info('Invalis JSON in file :', err)
            res.send(err.message)
        })
}

const addEvent = (req, res) => {
    res.render('new.html')
}

const getEvent = (req, res) => {
    const id = Number(req.params.eventID)
    fsp.readFile(filePath, 'utf8')
        .then(JSON.parse)
        .then((obj)=>{
            const ev = obj.find(event => event.id === id)
            if (!ev) {
                return res.status(404).send('Event not found')
            }
            res.render('detail.html', {ev})
        })
        .catch((err)=>{
            log.info('Invalis JSON in file :', err)
            res.send(err.message)
        })
}

const createEvent = (req, res) => {
    const newEvent = {
        id: events.length + 1,
        subject: req.body.subject,
        date: req.body.date
    }

    fsp.readFile(filePath, 'utf8')
        .then(JSON.parse)
        .then((eve)=>{

            //res.send(events)
            eve.push(newEvent)

            const json = JSON.stringify(eve)
            fsp.writeFile(filePath, json, 'utf8')
                .catch((err)=>{
                    log.info('Invalis JSON in file :', err)
                    res.send(err.message)
                })
            res.redirect('/')
        })

        .catch((err)=>{
            log.info('Invalis JSON in file :', err)
            res.send(err.message)
        })
}

const updateEvent = (req, res) => {
    const id = Number(req.params.eventID)
    fsp.readFile(filePath, 'utf8')
        .then(JSON.parse)
        .then((even)=>{

            const index = even.findIndex(event => event.id === id)
            const updatedEvent = {
                id,
                subject: req.body.subject,
                date: req.body.date
            }

            events[index] = updatedEvent
            const json = JSON.stringify(even)
            fsp.writeFile(filePath, json, 'utf8')
                .catch((err)=>{
                    log.info('Invalis JSON in file :', err)
                    res.send(err.message)
                })
            res.redirect('/')

        })
        .catch((err)=>{
            log.info('Invalis JSON in file :', err)
            res.send(err.message)
        })
}

const deleteEvent = (req, res) => {
    const id = Number(req.params.eventID)

    fsp.readFile(filePath, 'utf8')
        .then(JSON.parse)
        .then((eventt)=>{

            const e = eventt.findIndex(event => event.id === id)

            eventt.splice(e, 1)
            const json = JSON.stringify(eventt)
            fsp.writeFile(filePath, json, 'utf8')
                .catch((err)=>{
                    log.info('Invalis JSON in file :', err)
                    res.send(err.message)
                })
            res.redirect('/')

        })
        .catch((err)=>{
            log.info('Invalis JSON in file :', err)
            res.send(err.message)
        })
}

module.exports = {
    getEvents,
    getEvent,
    createEvent,
    updateEvent,
    deleteEvent,
    addEvent
}

// async-callback
/*
app.get('/events-callback', (req, res) => {
fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
        log.error(err)
        res.send('File not found')
        return
    } else {
        try {
            const obj = JSON.parse(data)
            res.send(obj)
        } catch (e) {
            log.info('Invalis JSON in file :', e)
            res.send(e.message)
        }

    }
})

})
// async-promise-then
//Exécution asynchrone par promesse
app.get('/events-promise', (req, res) => {
fsp.readFile(filePath, 'utf8')
    .then(JSON.parse)
    .then((obj)=>{
        res.send(obj)
    })
    .catch((err)=>{
        log.info('Invalis JSON in file :', err)
        res.send(err.message)
    })

})
// async-promise-async-await
//Exécution asynchrone/await
app.get('/events-await', (req, res) => {
    async function readFiles() {
        try {
            return await fsp.readFile(filePath, 'utf8')
        } catch (e) {addEvent
            return e
        }
}
readFiles().then(resp=>res.send(resp)).catch(error=>log.error(error))

})

app.post('/events-post-callback', (req, res) => {
const content = req.body
fs.readFile(filePath, 'utf8', (err, data)=>{
    if (err) {
        log.info(err)
        res.send('file not found')
        return
    } else {

        const obj = JSON.parse(data) //now it an object
        res.send(obj)
        obj.push(content)
        const json = JSON.stringify(obj) //convert it back to json
        fs.writeFile(filePath, json, 'utf8', (error)=> {
            if (error) {
                return log.info(error)
            }
        })
    }
})

})

app.post('/events-post-promesse', (req, res) => {
const content = req.body

fsp.readFile(filePath, 'utf8')
    .then(JSON.parse)
    .then((obj)=>{
        res.send(obj)
        obj.push(content)
        const json = JSON.stringify(obj)
        fsp.writeFile(filePath, json, 'utf8')
            .catch((err)=>{
                log.info('Invalis JSON in file :', err)
                res.send(err.message)
            })

    })

})

app.listen(port, () => {
    log.info(Example app listening at http://localhost/:${port})
})
*/
