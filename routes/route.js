'use strict'
const express = require('express')
const router = new express.Router()

const {
    getEvents,
    getEvent,
    createEvent,
    updateEvent,
    deleteEvent,
    addEvent
} = require('../controllers/events')

router.get('/', getEvents)

router.get('/addEvent', addEvent)

router.get('/detail/:eventID', getEvent)

router.post('/', createEvent)

router.post('/edit/:eventID', updateEvent)

router.get('/delete/:eventID', deleteEvent)

module.exports = router
