'use strict'
const express = require('express')
const app = express()
const port = 8082

const bunyan = require('bunyan')
const log = bunyan.createLogger({name: 'Agenda'})
const nunjucks = require('nunjucks')
const events_routes = require('./routes/route.js')
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.set('view engine', 'html')

nunjucks.configure(['views/'], {
    autoescape: false,
    express:app
})

app.listen(port, () => {
    log.info('server is listening on port "8082"')
})

app.use(express.json())
app.use('/', events_routes)
module.exports = app
