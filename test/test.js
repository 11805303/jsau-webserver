'use strict'
const app = require('../app')
const request = require('supertest')

/* eslint-env jest /
/eslint quote-props: [2, "always"]/
/eslint-env es6*/
//const jest = require('jest')
//const it = jest.it
//const describe = jest.describe

describe('Events API', ()=>{

    //test GET route

    describe('GET /api/events/', ()=>{
        it('It should GET all the events', (done)=>{
            request(app)
                .get('/')
                .then(response=>{
                    expect(response.statusCode).toEqual(200)
                    done()
                })
        })
    })

    describe('POST /api/events', () => {
        it('It should create a new event', async()=> {
            const event = {
                id:3,
                subject:'event3',
                date:'16/11/2021'
            }
            const res = await request(app)
                .post('/')
                .send(event)
            expect(res.statusCode).toEqual(302)
        })
    })
    /*
    describe('POST /api/events', () => {
    it('It should POST a new event',async()=>{
        const event ={
            id:3,
            subject:'event3',
            date:'16/11/2021'
        }
            .post('/api/events')
            .send(event)
            .expect(201)
            .end((err,res) => {
                if (err) {
                    return done(err)
                }
                res.status=201
                res.body.subject='event3'
                res.body.date='16/11/2021'
                done();
            })
    })
})*/

    describe('POST /edit/:eventID', () => {
        it('It should PUT a new event', async()=>{
            const eventID = 1
            const event = {
                subject:'event1 updated',
                date:'16/11/2021'
            }
            const res = await request(app)
                .post('/edit/' + eventID)
                .send(event)
            expect(res.statusCode).toEqual(302)

        })
    })

    describe('GET /delete/:eventID', () => {
        it('It should PUT a new event', async()=>{
            const eventID = 1

            const res = await request(app)
                .get('/delete/' + eventID)

            expect(res.statusCode).toEqual(302)

        })
    })

})